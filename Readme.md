# Using and validating signed container images in kubernetes / OKD / OpenShift

***Q2 / 2022:***

We are now entering the second phase for the configurations. We've decided to go with the following
configuration in direction production readyness:

- Connaisseur as running controller in the cluster
- cosign for asset signature (OCI images for now)
- establish a non connected signing and verification process
- as we see OKD / OpenShift has always the tightest rules depending on security and installation, though 
  we try OKD / OpenShift (>= 4.6) first.

Though we are sunsetting the old configs. There are accessible with the git-tag: `Finish-Phase-I`

## install connaisseur in your cluster

### OKD / OpenShift 4

What are we doing?

Install connaisseur on the cluster in the namespace `infra-connaisseur` via helm and setting the validation policies to: `validate`. This means that you have to add the label `securesystemsengineering.connaisseur/webhook=validate` to enable connaisseur validation. This should be made for every normal user, project, develop namespace, but not for the system namespaces in the first phase.

Connaisseur installation:
- login with cluster admin rights
- create namespace: `oc new-project infra-connaisseur`
- edit [connaisseur-ocp-values.yaml](./connaisseur-ocp-values.yaml) (or at least check it)
- install from here with:  
  `helm install sigpoc connaisseur/helm --atomic --namespace infra-connaisseur -f connaisseur-ocp-values.yaml`
- Add the following label to the namespaces / projects you want to enable: `oc label ns/mysupernamespace securesystemsengineering.connaisseur/webhook=validate`


## CoSign 

### Connected test (only works with internet access for cmd-line and cluster)

In CoSign the signatures are assets beside the OCI images and don't require a special service. So you will see strange assets beside your container in harbor for example.

I've made a small script [make-cosign-key.sh](./make-cosign-key.sh) to help your generate different keys. Just invoke it with your key name: `./make-cosign-key.sh mykey`

Sign: `cosign sign -key cosign-certs/master.key reg.pflaeging.net/sig-poc/pflaeging-net-ubi-debug-cosign:latest`

Verify:

```shell
cosign verify -key cosign-certs/master.pub reg.pflaeging.net/sig-poc/pflaeging-net-ubi-debug-cosign:latest | jq .
``` 

Output:
```JSON
{
  "critical": {
    "identity": {
      "docker-reference": "reg.pflaeging.net/sig-poc/pflaeging-net-ubi-debug-cosign"
    },
    "image": {
      "docker-manifest-digest": "sha256:ed984c3a4e006af0ceffe1b091198acfa7839e60b9eb53af5739674f0f8b5d3e"
    },
    "type": "cosign container image signature"
  },
  "optional": null
}
```

### Disconnected setup (setup with cmd-line and cluster in island mode)

TBD (pflaeging 2022-07-04)

